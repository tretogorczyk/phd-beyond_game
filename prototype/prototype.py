import random
from logging import DEBUG, log, basicConfig, info
import json
import threading
import copy
import time
import os

from render import drawing, opengl
from event import manager
from map import map
import schema
import utils.logger
import config.config_parser as config

FORMAT = f'%(asctime)-15s {__name__: <10}   : %(message)s'
basicConfig(format=FORMAT, level=DEBUG)

class GridDefinition():
    def __init__(self, width, height, world_schema):
        log(DEBUG, f'creating a grid {width}width by {height}height')
        self.width = width
        self.height = height
        self.cells = []
        self.world_schema = world_schema
        cell_type_number = world_schema.get_cell_type_number()
        for i in range(0, height):
            self.cells.append([world_schema.get_random_block_for_cell(random.randint(0, cell_type_number - 1)) for j in range(0, width)])
            #find a more efficient way for this

    def get_new_row(self):
        cell_type_number = world_schema.get_cell_type_number()
        new_row = [world_schema.get_random_block_for_cell(random.randint(0, cell_type_number - 1)) for j in range(0, self.width)]
        return new_row

    def update_grid(self, direction):
        cell_type_number = world_schema.get_cell_type_number()
        if direction == "south":
            self.cells.pop(0)
            self.cells.append(self.get_new_row())
        if direction == "north":
            self.cells.pop(self.height - 1)
            self.cells.insert(0, self.get_new_row())
        if direction == "east":
            for i in range(0, self.height):
                self.cells[i].pop(0)
                self.cells[i].append(world_schema.get_random_block_for_cell(random.randint(0, cell_type_number - 1)))
        if direction == "west":
            for i in range(0, self.height):
                self.cells[i].pop(-1)
                self.cells[i].insert(0, world_schema.get_random_block_for_cell(random.randint(0, cell_type_number - 1)))

# class Cell:
#     """
#     A single cell
#     """
#     def __init__(self, cell_definition):
#         self.cell_type = cell_definition["name"]
#         self.walkable = cell_definition["walkable"]


def start_2D_drawer(map):
    drawer = drawing.Drawer2DConsole()
    drawer_thread = drawing.DrawerThread(map, drawer)
    drawer_thread.start()


def start_OpenGL_drawer(event_manager, map):
    window = 0  # glut window number
    width, height = 550, 550  # window size
    columns, rows = config.width, config.height
    drawer = opengl.DrawerOpenGL(event_manager, map, columns, rows, width, height)
    drawer.start()


file_location = "config/world_schema.json"
world_schema = schema.WorldSchema(file_location)
event_manager = manager.EventManager()
event_manager.start()

world_map = map.Map(event_manager, world_schema)
character = map.Character(int(config.width / 2), int(config.height / 2))
world_map.add_character(character)
init_tile = world_map.generate_tile(1, config.width, config.height, "random")
world_map.add_path(1, 2, init_tile)
world_map.start()


START_GAME = 1
if START_GAME:
    i = 0
    start_OpenGL_drawer(event_manager, world_map)
    event_manager.start()
    while i < 55:
        i += 1
        movement = 5
        # game_tile = map.get_current_tile_cells()
        time.sleep(0.1)
        movement = input()
        log(DEBUG, f'movement: {movement}')
        world_map.process_events()
        world_map.move_character(movement)



# TUNOWANIE GENERATORA ZA POMOCA SIECI REKURENCYJNEJ Z ESTYMATA STANU? BO WIEMY ZE WPLYNELISMY NA SWIAT HA
