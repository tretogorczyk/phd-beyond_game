import threading
import time

from utils.logger import log, info, DEBUG


class EventManager(threading.Thread):
    EVENT_KEY_PRESSED = 55
    def __init__(self):
        log(DEBUG, f'instantiating event thread')
        super(EventManager, self).__init__(target=self.event_thread, args=(1,))
        self.run_event_manager = True
        self.events = []
        self.event_callbacks = []

    def event_thread(self, _):
        log(DEBUG, f'starting event thread')
        while self.run_event_manager:
            while self.events:
                log(DEBUG, f"event loop - parsing events")
                event = self.events.pop()
                for cb_client in self.event_callbacks:
                    if cb_client[0] == event[0]:
                        cb_client[1](event[1])
            time.sleep(0.2)

    def send_event(self, event_type, *argv):
        log(DEBUG, f'received event notification for event {event_type}')
        log(DEBUG, f'args {[arg for arg in argv]}')
        self.events.append((event_type, argv))

    def register_for_event(self, event_type, callback):
        log(DEBUG, f'registering for {event_type} callback {callback}')
        self.event_callbacks.append((event_type, callback))

    # def register_signal(self, signal, ):
