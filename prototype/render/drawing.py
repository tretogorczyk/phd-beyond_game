import threading
import time
import os
from abc import ABC, abstractmethod


from logging import DEBUG, log, basicConfig
FORMAT = f'%(asctime)-15s {__name__: <10}   : %(message)s'
basicConfig(format=FORMAT, level=DEBUG)


class Drawer:
    def __init__(self):
        pass

    @abstractmethod
    def draw(self, map_tile):
        pass


class Drawer2DConsole(Drawer):
    """
    Implementation of a 2D Console printing drawer
    """
    def __init__(self):
        pass

    def draw(self, map_tile):
        width = map_tile.width
        pixmap = map_tile.get_pixel_map()
        [print(f'{"|".join(pixmap[(row_id * width):((row_id + 1) * width)])}') for row_id in
         range(0, map_tile.height)]


class DrawerThread(threading.Thread):
    def __init__(self, map_handle, drawer):
        log(DEBUG, f'instantiating drawer thread')
        super(DrawerThread, self).__init__(target=self.drawer_thread, args=(1,))
        self.map_handle = map_handle
        self.drawer = drawer

    def drawer_thread(self, name):
        log(DEBUG, f'starting drawer thread')
        while 1:
            os.system("clear")
            game_tile = self.map_handle.get_current_tile_cells()
            self.drawer.draw(game_tile)
            log(DEBUG, f'drawer thread {name}: iteration')
            time.sleep(0.1)
