import time
import os
import threading

from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *

from event import manager

from logging import DEBUG, log, basicConfig
FORMAT = f'%(asctime)-15s {__name__: <10}   : %(message)s'
basicConfig(format=FORMAT, level=DEBUG)

class TileGL:
    def __init__(self, left_x, bot_y):
        self.left_x = left_x
        self.bot_y = bot_y

def translate_name_to_color(pixel):
    if pixel == 'S':
        return 1.0, 1.0, 1.0
    elif pixel == 's':
        return 0.8, 0.8, 0.8
    elif pixel == 'U':
        return 0.0, 0.0, 1.0,
    elif pixel == 'u':
        return 0.0, 0.0, 0.8
    elif pixel == 'D':
        return 0.0, 1.0, 0.0
    elif pixel == 'd':
        return 0.0, 0.8, 0.0
    elif pixel == 'r':
        return 0.2, 0.2, 0.2
    elif pixel == 'P':
        return 0.0, 0.0, 0.0

def translate_block_names_to_colors(map_tile):
    pixmap = map_tile.get_pixel_map()
    colors = [translate_name_to_color(pixel) for pixel in pixmap]
    return colors


class TileVertIter:
    """Iterator that returns next tile x, y vertices."""
    def __init__(self, columns, rows, width, height):
        self.width = width
        self.height = height
        self.columns = columns
        self.rows = rows
        tile_width = width / columns
        tile_height = height / rows
        self.cur_row = 0
        self.cur_col = 0

        self.x_positions = [col * tile_width for col in range(0, columns)]
        self.y_positions = [row * tile_height for row in range(0, rows)]
        print(self.x_positions)
        print(self.y_positions)

    def __prepare_tile_vertices(self):
        tile_count = self.columns * self.rows
        # self.x_positions = col * tile_width for col in range(0, columns)

    def __iter__(self):
        return self

    def __next__(self):
        """
        :return: x, y coordinates for bottom left vertice
        """
        if self.cur_row == self.rows - 1:
            self.cur_row = 0
            if self.cur_col == self.columns - 1:
                self.cur_col = 0
            else:
                self.cur_col += 1
        else:
            self.cur_row += 1
        return self.x_positions[self.cur_row], self.y_positions[self.cur_col]


class DrawerOpenGL:
    def __init__(self, event_manager, map_handle, columns, rows, width, height):
        self.tile_iter = TileVertIter(columns, rows, width, height)
        self.width = width
        self.height = height
        self.columns = columns
        self.rows = rows
        self.tile_width = width / columns
        self.tile_height = height / rows
        self.tile_count = columns * rows
        self.map_handle = map_handle
        self.event_manager = event_manager

        # initialization
        glutInit()  # initialize glut
        glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)
        glutInitWindowSize(width, height)  # set window size
        glutInitWindowPosition(0, 0)  # set window position
        window = glutCreateWindow("prototype_game")  # create window with title
        self.registerCallbacks()
        glutDisplayFunc(self.draw)  # set draw function callback
        glutIdleFunc(self.draw)  # draw all the time

    def start(self):
        glutMainLoop()

    def draw(self):
        game_tile = self.map_handle.get_current_tile_cells()
        self.colors = iter(translate_block_names_to_colors(game_tile))
        self.draw_gl(game_tile)

    def get_color(self, tile_id):
        return next(self.colors)

    def draw_gl(self, game_tile):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  # clear the screen
        glLoadIdentity()  # reset position
        self.refresh2d(self.width, self.height)  # set mode to 2d
        # self.draw_rect(10, 10, self.tile_width, self.tile_height)  # rect at (10, 10) with width 200, height 100
        self.draw_tiles()

        glutSwapBuffers()  # important for double buffering

    def draw_tiles(self):
        for tile_id in range(0, self.tile_count):
            self.draw_rect(tile_id, self.tile_width, self.tile_height)  # rect at (10, 10) with width 200, height 100

    def draw_rect(self, tile_id, width, height):
        x, y = next(self.tile_iter)
        colors = self.get_color(tile_id)
        glColor3f(colors[0], colors[1], colors[2])
        # glColor3f(0.0, 0.0, 1.0)  # set color to blue
        glBegin(GL_QUADS)  # start drawing a rectangle
        glVertex2f(x, y)  # bottom left point
        glVertex2f(x + width, y)  # bottom right point
        glVertex2f(x + width, y + height)  # top right point
        glVertex2f(x, y + height)  # top left point
        glEnd()  # done drawing a rectangle

    def refresh2d(self, width, height):
        glViewport(0, 0, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(0.0, width, 0.0, height, 0.0, 1.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

    def keyPressed(self, key, x, y):
        print(f'key {key} pressed at {x}, {y}')
        self.event_manager.send_event(manager.EventManager.EVENT_KEY_PRESSED, key, x, y)


    def registerCallbacks(self):
        """Initialise glut settings concerning functions"""
        # glutMouseFunc(mouseButton)
        # glutMotionFunc(mouseMotion)
        glutKeyboardFunc(self.keyPressed)
        # glutSpecialFunc(specialKeyPressed)

# window = 0  # glut window number
# width, height = 550, 550  # window size
# columns, rows = 11, 11
#
# DrawerOpenGL(columns, rows, width, height)
# glutMainLoop()