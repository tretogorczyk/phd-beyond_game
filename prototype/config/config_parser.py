import random
import json
from utils.logger import log, DEBUG, info


class Config:
    def __init__(self, conf_file):
        log(DEBUG, f'reading configuration file')
        self.config = self.load_game_config(conf_file)
        self.parse_graphics()

    @staticmethod
    def load_game_config(file_location):
        with open(file_location, 'r') as schema_file:
            config = json.load(schema_file)
            info(f'loading game config from {file_location}')
            return config

    def parse_graphics(self):
        info(f'resolution used: {self.config["graphics"]["map_resolution"]}')

    def  get_resolution(self):
        return self.config["graphics"]["map_resolution"]["width"],  self.config["graphics"]["map_resolution"]["height"]


__config_file = "game_config.json"
__config = Config(__config_file)

width, height = __config.get_resolution()
