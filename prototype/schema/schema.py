import random
import json
from utils.logger import log, DEBUG, info

class WorldSchema:
    class CellDefinition():
        def __init__(self, name, block_types):
            self.name = name
            self.block_types = block_types

    def __init__(self, schema_file):
        log(DEBUG, f'instantiating world schema singleton')
        self.cell_definitions = None
        self.schema_file = schema_file
        self.schema = self.load_world_schema(schema_file)
        self.parse_cell_type_definition(self.schema["cell_definition_collection"])

    @staticmethod
    def load_world_schema(file_location):
        with open(file_location, 'r') as schema_file:
            schema = json.load(schema_file)
            info(f'loading world schema from {file_location}')
            return schema

    def parse_cell_type_definition(self, cell_definition_collection):
        self.cell_definitions = [WorldSchema.CellDefinition(cell_def["name"], cell_def["blocks"]) for cell_def in cell_definition_collection]
        info(f'found block definitions: {[cell.name for cell in self.cell_definitions]}')

    def parse_block_type_definition(self, cell_definition):
        pass

    def get_cell_type_number(self):
        return len(self.cell_definitions)

    def get_random_cell(self):
        """
        Returns a cell of random type, one of types in world schema
        :return: random cell type
        """
        return random.randint(0, len(self.cell_definitions) - 1)

    def get_random_block_for_cell(self, cell_type):
        """
        Given a cell type, returns a random block type. A cell (e.g. water, sand, dirt) can have multiple blocks
        :param cell_type: type of cell to generate a bock
        :return: ramdom block of the given cell type
        """
        cell_definition = self.cell_definitions[cell_type]
        block_type_id = random.randint(0, len(cell_definition.block_types) - 1)
        return block_type_id
        #return (cell_definition.block_types[random.randint(0, len(cell_definition.block_types) - 1)])["name"]

    def get_block_name_property(self, cell_type, block_type_id):
        # print(self.cell_definitions[cell_type].block_types[block_type_id]["name"])
        return self.cell_definitions[cell_type].block_types[block_type_id]["name"]

    def get_block_walkable_propety(self, cell_type, block_type_id):
        return self.cell_definitions[cell_type].block_types[block_type_id]["walkable"]

    # def get_random_block(self, cell_definition_name):
    #     for cell in self.cell_definitions:
    #         if cell_definition_name == cell.name:
    #             return cell.block_types[random.randint(0, len(cell.block_types))]
    #     return "404"

    # HOW DO WE OVERLOAD EH?
    # def get_random_block(self, cell_definition_id):
    #     cell_definition = self.cell_definitions[cell_definition_id]
    #     return cell_definition.block_types[random.randint(0, len(cell_definition.block_types)-1)]
