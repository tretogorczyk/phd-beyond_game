import threading
import copy
import time

from utils.logger import log, info, DEBUG
from event import manager

class Character:
    def __init__(self, pos_x, pos_y):
        log(DEBUG, f'instantiating Character at {pos_x},{pos_y}')
        self.pos_x = pos_x
        self.pos_y = pos_y

    def get_position(self):
        return self.pos_x, self.pos_y

    def update_position(self, new_pos_x, new_pos_y):
        self.pos_x = new_pos_x
        self.pos_y = new_pos_y

class Tile:
    """
    A dedicated map tile. A tile is made of multiple columns and rows of cells. Each cell has a type and can be created
    using one of the block types that this cell type supports (e.g. a cell type water can have multiple water blocks)
    """
    def __init__(self, tile_type, width, height, world_schema):
        log(DEBUG, f'generating a tile with type {tile_type}')
        self.height = height
        self.width = width
        self.pixel_cnt = height * width
        self.tile_type = tile_type
        self.__generate(world_schema)

    def __generate(self, world_schema):
        print("generating...")
        self.block_types = [world_schema.get_random_block_for_cell(self.tile_type) for pix in range(0, self.pixel_cnt)]
        self.pixel_map = [world_schema.get_block_name_property(self.tile_type, block_type_id) for block_type_id in self.block_types]
        self.walkable_map = [world_schema.get_block_walkable_propety(self.tile_type, block_type_id) for block_type_id in self.block_types]
        self.portal_map = [0 for pix in range(0, self.pixel_cnt)]

    #def __getattr__(self, item):
    def get_pixel_map(self):
        """
        Tiles are now essentially a list of lists:
        [row0, row1, row2 ... height-1] where each row is [cell0, cell1, cell2, ... width-1]
        :return:
        """
        return self.pixel_map

    def set_cell(self, pos_x, pos_y, value):
        self.pixel_map[pos_y * self.width + pos_x] = value

    def get_walkable(self, pos_x, pos_y):
        return self.walkable_map[pos_y * self.width + pos_x]

    def get_portal(self, pos_x, pos_y):
        return self.portal_map[pos_y * self.width + pos_x]

class TileFactory:
    def __init__(self, world_schema):
        log(DEBUG, f'instantiating Tile Factory singleton')
        self.world_schema = world_schema

    def generate_tile(self, tile_type,  width, height, generation_method):
        tile = Tile(tile_type, width, height, self.world_schema)
        return tile


class PathGenerator:
    """
    Class generating walkable paths on and between grids
    """
    def __init__(self, world_schema):
        self.world_schema = world_schema

    def add_path(self, in_dir, out_dir, map_tile):
        height = map_tile.height
        width = map_tile.width
        middle_height = int(height / 2)
        pixel_map = map_tile.get_pixel_map()

        for row_id in range(0, height):
            pixel_map[row_id * width + middle_height] = "r"
            map_tile.walkable_map[row_id * width + middle_height] = 1
            if row_id == 0 or row_id == (height-1):
                map_tile.portal_map[row_id * width + middle_height] = 1

class Map(threading.Thread):
    """
    Class controlling the overall map of the system
    """
    def __init__(self, event_manager, world_schema):
        super(Map, self).__init__(target=self.process_events)
        log(DEBUG, f'instantiating Map singleton ')
        self.event_manager = event_manager
        self.world_schema = world_schema
        self.tiles = []
        self.tile_factory = TileFactory(world_schema)
        self.current_tile = None
        self.path_generator = PathGenerator(world_schema)
        self.event_manager.register_for_event(manager.EventManager.EVENT_KEY_PRESSED, self.add_key_to_process)
        self.keys_to_process = []

    def add_character(self, character):
        self.character = character

    def add_key_to_process(self, *args):
        log(DEBUG, f'received event notification with args {args}')
        key = args[0][0]
        self.keys_to_process.append(key)

    def process_events(self):
        while 1:
            while self.keys_to_process:
                log(DEBUG, f'processing key event in Map thread')
                key = self.keys_to_process.pop()
                self.move_character(key)
            time.sleep(0.2)

    def generate_tile(self, tile_type,  width, height, generation_method):
        """
        Generates a new tile and adds it to local list of tiles
        :param self: handle to self
        :param tile_type: type of the new tile as supported by schema
        :param width: width of the tile
        :param height: height of the tile
        :param generation_method: how to generate cells in a tile, e.g. randomize
        :return: handle to new tile
        """
        new_tile = self.tile_factory.generate_tile(tile_type, width, height, generation_method)
        log(DEBUG, f'factory produced new tile, total tile count: {len(self.tiles)}')
        self.tiles.append(new_tile)
        if self.current_tile is None:
            self.current_tile = new_tile
        return new_tile

    def add_path(self, in_dir, out_dir, map_tile):
        self.path_generator.add_path(in_dir, out_dir, map_tile)

    # def __getattr__(self, item):

    def get_current_tile_cells(self):
        buffered_tile = copy.deepcopy(self.current_tile)
        self.__add_character_to_tile(buffered_tile)
        return buffered_tile

    def __add_character_to_tile(self, tile):
        tile.set_cell(self.character.pos_x, self.character.pos_y, "P")
        return tile

    def is_cell_walkable(self, pos_x, pos_y):
        return self.current_tile.get_walkable(pos_x, pos_y)

    def is_outside_of_map(self, pos_x, pos_y):
        return pos_y == self.current_tile.height or pos_y < 0 or pos_x == self.current_tile.width or pos_x < 0

    def is_cell_portal(self, curr_pos_x, curr_pos_y):
        return self.current_tile.get_portal(curr_pos_x, curr_pos_y)

    def transition_to_new_tile(self, pos_x, pos_y):
        self.current_tile = self.generate_tile(1, self.width, self.height, "random")
        if pos_x == self.current_tile.width:
            pos_x = 0
        elif pos_x < 0:
            pos_x = self.current_tile.width - 1
        if pos_y == self.current_tile.height:
            pos_y = 0
        elif pos_y < 0:
            pos_y = self.current_tile.height - 1    
        self.character.update_position(pos_x, pos_y)
        self.add_path(1, 2, self.current_tile)

    def move_character(self, movement):
        # TODO: y position is swapped (w is down, s is up) because of rendering
        print(f"request to move with {movement}")
        character_pos_x = self.character.pos_x
        character_pos_y = self.character.pos_y
        if movement == b's':
            character_pos_y -= 1
        elif movement == b'a':
            character_pos_x -= 1
        elif movement == b'd':
            character_pos_x += 1
        elif movement == b'w':
            character_pos_y += 1
        if self.is_outside_of_map(character_pos_x, character_pos_y):
            log(DEBUG, "tile outside of tile range")
            if self.is_cell_portal(self.character.pos_x, self.character.pos_y):
                log(DEBUG, "new tile transition...")
                self.transition_to_new_tile(character_pos_x, character_pos_y)
                return
        if self.is_cell_walkable(character_pos_x, character_pos_y):
            log(DEBUG, "moving character...")
            self.character.update_position(character_pos_x, character_pos_y)


class MapControllerContract:
    def __init__(self):
        log(DEBUG, f'initializing MapController sigleton')

    def generate_initial_tile(self):
        """
        Generates an initial map, a  starting location with neutral conotation
        :return:
        """

    def generate_next_tile_with_mood(self, mood):
        """
        Given a specific mood of the tile (dark, light, sun, etc.) creates a full 2D tile
        :param mood:
        :return:
        """