from logging import DEBUG, log, basicConfig, info

FORMAT = f'%(asctime)-15s {__name__: <10}   : %(message)s'
basicConfig(format=FORMAT, level=DEBUG)


